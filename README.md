# Prueba de Concepto IoT

Este repositorio provee la creación de un entorno virtualizado para llevar adelante una Prueba de Concepto de Internet de las Cosas con herramientas libres.

Está compuesto por 4 máquinas virtuales:

* sensor.iot.local: provee dos juegos de sensores virtuales que producen datos al azar. Un juego de sensores de datos simples, y otro juego de sensores que producen datos en formato JSON.
* broker.iot.local: provee un concentrador MQTT (mosquitto), en conjunto con un adaptador de orígenes de datos no-MQTT, implementado con Node-RED
* dashboard.iot.local: provee dos tipos de paneles de control, implementados mediante Node-RED y con Freeboard. El panel de Node-RED provee una simulación de un sistema de ventilación con comunicación MQTT, para encender o apagar un juego de ventiladores.
* dataserver.iot.local: provee almacenamiento persitente para los mensajes que llegan al concentrador. Utilizando Node-RED como gateway, almacena mensajes en un motor SQL (MariaDB) como NoSQL (Redis).

Está basado en Debian GNU/Linux 9, utilizando Vagrant con aprovisionamiento mediante Ansible.

Se requiren los siguientes plugins de Vagrant:
* vagrant-hostmanager (1.8.8)
* vai (0.9.3)

## Arquitectura

A continuación podemos ver la arquitectura desplegada.

![Diagrama de despliegue](https://gitlab.com/flazcano/vagrant-iot/raw/develop/img/IoT_componentes_arquitectura.png)

## Opciones de administración más comunes

### sensor

#### Sensores virtuales JSON

Los sensores virtuales JSON se encuentran implementados mediante una aplicación Node.js que está instalada en /home/iot/.

Se invocan 3 servicios mediante systemd, para generar tres demonios que emiten datos hacia el broker.

Se pueden detener y volver a iniciar mediante systemctl stop virtual-sensor-N.

#### Sensores virtuales simples

Los sensores virtuales simples se encuentran implementados mediante simples script bash que utilizan el cliente mosquito para transmitir la información al broker.

También se pueden gestionar mediante systemd.

### broker

El broker está implementado mediante Mosquitto, el cual está configurado desde /etc/mosquitto/conf.d/*

La única modificación realizada a la configuración por defecto es el agregado de websockets, para integración con otros sistemas.

Se puede gestionar como cualquier otro demonio en Debian GNU/Linux.

Se encuentra instalado también Node-RED como adaptador de protocolos para que sean reinyectados en Mosquitto y de esta manera asegurar la estandarización de los mensajes.

Pueden encontrarse en http://broker.iot.local:1880

Para monitorear el estado del broker y los mensajes, se encuentra instalada la interfaz web MQTT Admin, disponible en http://dashboard.iot.local/mqtt-admin

### dashboard

El dashboard está compuesto por dos herramientas, Node-RED y Freeboard.
Los paneles de control de Node-RED son editables por defecto, con lo cual desde la misma interfaz de Node-RED es posible adaptarlos a otros usos.

Freeboard soporta múltiples paneles en una misma instalación, con lo cual, es necesario llamar al archivo index.html agregando la carga del panel que queremos invocar, en este caso iot.json. El archivo original de iot.json está en el directorio ansible/files/freeboard-iot.json.

### dataserver

En el servidor de persistencia de datos se han implementado dos sistemas de bases de datos, una SQL (MariaDB) y otra NoSQL (Redis).

Ambas soluciones se gestionan mediante Node-RED, quien realiza la conexión al broker MQTT, adapta los mensajes recibidos y los inyecta en los respectivos motores de bases de datos.

Los servicios mariadb, redis y node-red se gestionan mediante systemd.

## Listado de URL

### broker

* http://broker.iot.local/mqtt-admin
* http://broker.iot.local:1880

### dashboard

* http://dashboard.iot.local/freeboard/index.html?load=iot.json
* http://dashboard.iot.local:1880
* http://dashboard.iot.local:1880/ui/

### dataserver

* http://dataserver.iot.local:1880/
* http://dataserver.iot.local/phpmyadmin/
* http://dataserver.iot.local:8081
