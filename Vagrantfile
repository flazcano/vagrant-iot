# -*- mode: ruby -*-
# vim: ft=ruby

# Vagrant plugins requeridos:
# vagrant-hostmanager
# vai

# Vagrant plugins opcionales:
# vagrant-vbguest

# ---- Configuration variables ----
GUI = false # Enable/Disable GUI
RAM = 384   # Default memory size in MB

# Network configuration
DOMAIN = "iot.local"
NETWORK = "192.168.99"
NETMASK = "255.255.255.0"

# Default Virtualbox box
# See: https://wiki.debian.org/Teams/Cloud/VagrantBaseBoxes
BOX = 'debian/contrib-stretch64'

HOSTS = {
#--"hostname" => ["IP", RAM, GUI, BOX],
   "sensor" => ["10", RAM, GUI, BOX],
   "broker" => ["11", RAM, GUI, BOX],
   "dashboard" => ["12", RAM, GUI, BOX],
   "dataserver" => ["13", RAM, GUI, BOX],
}

ANSIBLE_INVENTORY_DIR = 'ansible'
ANSIBLE_INVENTORY_FILE = 'inventory'

# ---- Vagrant configuration ----

Vagrant.configure(2) do |config|

  if Vagrant.has_plugin?("vagrant-hostmanager")
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.manage_guest = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true
  end

  HOSTS.each do | (name, cfg) |
    ipaddr, ram, gui, box = cfg

    config.vm.define name do |machine|
      machine.vm.box   = box
      machine.vm.guest = :debian

      machine.vm.provider "virtualbox" do |vbox|
        vbox.gui    = gui
        vbox.memory = ram
        vbox.name = name
      end

      machine.vm.hostname = name + '.' + DOMAIN
      machine.vm.network 'private_network', ip: NETWORK + '.' + ipaddr, netmask: NETMASK

      if Vagrant.has_plugin?("vagrant-hostmanager")
        machine.hostmanager.aliases = %W(#{name + '.' + DOMAIN + ' ' + name} )
      end

    end # config.vm

  end # HOSTS-each

  config.vm.provision "vai" do |ansible|
    ansible.inventory_dir=ANSIBLE_INVENTORY_DIR
    ansible.inventory_filename=ANSIBLE_INVENTORY_FILE
    ansible.groups = {
      'sensors' => [ 'sensor' ],
      'brokers' => [ 'broker' ],
      'dashboards' => [ 'dashboard' ],
      'dataservers' => [ 'dataserver' ],
    }
  end

end
