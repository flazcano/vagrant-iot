#!/bin/bash

PAUSE=5
BROKER=broker.iot.local



while true; do

	TEMPERATURA=`shuf -i20-25 -n1`
	HUMEDAD=`shuf -i10-100 -n1`
	PRESION=`shuf -i1050-1150 -n1`

	mosquitto_pub -h ${BROKER} -m ${TEMPERATURA} -t sensors/temperatura-simple/data ;
	mosquitto_pub -h ${BROKER} -m ${HUMEDAD} -t sensors/humedad-simple/data ;
	mosquitto_pub -h ${BROKER} -m ${PRESION} -t sensors/presion-simple/data ;
	
	sleep ${PAUSE};
done

