---

- name: Instalación de Dashboards
  hosts: dashboard
  vars_files:
    - vars/main.yml
  become: true
  tasks:

    - name: Instalamos servidor Apache
      apt: pkg={{item}} state=latest
      with_items:
        - apache2

    # Instalación y configuración de Freeboard

    - name: Instalamos freeboard.io desde git
      git: repo=git://github.com/Freeboard/freeboard.git
           accept_hostkey=yes
           dest={{ install_path }}/freeboard
           force=yes

    - name: Descargamos librería Eclipse Paho MQTT para freeboard, ver https://www.eclipse.org/paho/clients/js/
      get_url:
        url: https://cdnjs.cloudflare.com/ajax/libs/paho-mqtt/1.0.1/mqttws31.js
        dest: "{{ install_path }}/freeboard/plugins/"

    - name: Instalamos plugin MQTT para freeboard desde git
      get_url:
        url: https://raw.githubusercontent.com/alsm/freeboard-mqtt/master/paho.mqtt.plugin.js
        dest: "{{ install_path }}/freeboard/plugins/"

    - name: Utilizamos versión local de librería Paho en el plugin MQTT
      replace:
        path: "{{ install_path }}/freeboard/plugins/paho.mqtt.plugin.js"
        regexp: 'https://rawgit.com/benjaminchodroff/freeboard-mqtt/paho-mqtt-default/mqttws31.js'
        replace: 'plugins/mqttws31.js'

    - name: Habilitamos el plugin MQTT en index.html de Freeboard
      replace:
        path: "{{ install_path }}/freeboard/index.html"
        regexp: '"js/freeboard_plugins.min.js",'
        replace: '"js/freeboard_plugins.min.js", "plugins/paho.mqtt.plugin.js", '

    - name: Instalamos plugin MQTT2 para freeboard desde git
      get_url:
        url: https://raw.githubusercontent.com/wwolkers/freeboard-websocket-mqtt/master/ws_mqtt.js
        dest: "{{ install_path }}/freeboard/plugins/"

    - name: Habilitamos el plugin MQTT2 en index.html de Freeboard
      replace:
        path: "{{ install_path }}/freeboard/index.html"
        regexp: '"js/freeboard_plugins.min.js",'
        replace: '"js/freeboard_plugins.min.js", "plugins/ws_mqtt.js", '

    - name: Instalamos plugin highcharts desde https://github.com/onlinux/freeboard-plugins/blob/master/plugin_highcharts.js
      get_url:
        url: https://raw.githubusercontent.com/onlinux/freeboard-plugins/master/plugin_highcharts.js
        dest: "{{ install_path }}/freeboard/plugins/"

    - name: Habilitamos el plugin highcharts en index.html de Freeboard
      replace:
        path: "{{ install_path }}/freeboard/index.html"
        regexp: '"js/freeboard_plugins.min.js",'
        replace: '"js/freeboard_plugins.min.js", "plugins/plugin_highcharts.js", '

    - name: Instalamos plugin slider desde https://raw.githubusercontent.com/onlinux/freeboard-plugins/master/slider-plugin.js
      get_url:
        url: https://raw.githubusercontent.com/onlinux/freeboard-plugins/master/slider-plugin.js
        dest: "{{ install_path }}/freeboard/plugins/"

    - name: Habilitamos el plugin slider en index.html de Freeboard
      replace:
        path: "{{ install_path }}/freeboard/index.html"
        regexp: '"js/freeboard_plugins.min.js",'
        replace: '"js/freeboard_plugins.min.js", "plugins/slider-plugin.js", '

    - name: Instalamos plugin switch desde https://raw.githubusercontent.com/onlinux/freeboard-plugins/master/switch.js
      get_url:
        url: https://raw.githubusercontent.com/onlinux/freeboard-plugins/master/switch.js
        dest: "{{ install_path }}/freeboard/plugins/"

    - name: Habilitamos el plugin switch en index.html de Freeboard
      replace:
        path: "{{ install_path }}/freeboard/index.html"
        regexp: '"js/freeboard_plugins.min.js",'
        replace: '"js/freeboard_plugins.min.js", "plugins/switch.js", '

    - name: Copiamos tablero inicial para Freeboard
      copy: src=files/freeboard-iot.json dest={{ install_path }}/freeboard/iot.json owner=iot group=iot

    - name: Creamos enlace para habilitar freeboard en Apache
      file:
        src: "{{ install_path }}/freeboard"
        dest: "/var/www/html/freeboard"
        state: link

    - name: Ajustamos permisos para permitir la edición desde Apache
      file: path={{ install_path }}/freeboard owner={{ run_as }} group=www-data recurse=yes

    # Configuración de Node-RED
    - name: Instalamos módulos de visualización para Node-RED
      npm: name={{ item }} path={{ install_path }}/node-red/
      with_items:
        - node-red-dashboard

    - name: Copiamos tablero inicial para Node-RED
      copy: src=files/node-red-dashboard-iot.json dest={{ install_path }}/.node-red/flows_dashboard.json owner=iot group=iot

    - name: Reiniciamos Node-RED
      systemd:
        state: restarted
        name: node-red
